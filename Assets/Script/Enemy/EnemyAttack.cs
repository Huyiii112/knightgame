﻿using UnityEngine;

public class EnemyAttack : MonoBehaviour
{
    private Enemy m_enemy;
    public float attackCooldown;
    private float m_currentTime;
    private bool m_canAttack;
    private void Start()
    {
        m_enemy = GetComponentInParent<Enemy>();
    }
    private void Update()
    {
        m_currentTime += Time.deltaTime;
        if (m_currentTime >= attackCooldown)
        {
            m_canAttack = true;
            
        }
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.CompareTag("Player") && m_canAttack)
        {
            m_enemy.AttackPlayer();
            m_canAttack = false;
            m_currentTime = 0;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            m_enemy.ExitAttack();
            m_canAttack = false;
        }
    }
}
