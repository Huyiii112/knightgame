﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static TMPro.Examples.TMP_ExampleScript_01;

public class Enemy : MonoBehaviour
{
    public int HP;
    public int def_ad;
    public int def_ap;
    public int dmg;
    public Slider hpbar;
    public float moveSpeed = 2f;
    public Transform pointA, pointB;
    public int damage = 10;
    public DIsplayDmg dmgCanva;
    public Transform pontUiDmg;
    public float attackCooldown = 1f;
    public Vector3 scale;
    public ObjectType objectType;
    public GameObject bulletAtk;
    public int expReward;
    public int goldReward;
    public EnemySpawn e;

    [SerializeField]
    private int m_currentHp;
    private EnemyAnimation m_enemyAnimation;
    private bool m_isMove = true;
    private GameObject m_player;

    public Vector3 m_posA, m_posB;
    public Vector3 startPos;
    private bool m_movingLeft = true;
    public float m_lastAttackTime = 0f;

    private void Start()
    {
        startPos = gameObject.transform.position;
        m_player = GameObject.FindGameObjectWithTag("Player");
        m_enemyAnimation = GetComponent<EnemyAnimation>();
        m_currentHp = HP;
        m_posA = startPos + new Vector3(-2f, 0f, 0f);
        m_posB = startPos + new Vector3(2f, 0f, 0f);
        hpbar.value = (float)m_currentHp / HP;
    }
    public void SetPosMove()
    {
        m_posA = startPos + new Vector3(-2f, 0f, 0f);
        m_posB = startPos + new Vector3(2f, 0f, 0f);
    }    
    private void Update()
    {
        Walk();

    }
    public void SetHp()
    {
        m_isMove = true;
        m_currentHp = HP;
        hpbar.value = (float)m_currentHp / HP;
    }
    public void DMG(SkillSO skillSO)
    {

        if (m_currentHp <= 0) return;

        if (skillSO == null) return;
        m_enemyAnimation.SetAniHurt();
        if (skillSO.skillType == SkillType.AttackDmg)
        {
            int dmg = PlayerController.instance.player.GetAttackDmg();
            dmg += skillSO.levelDamageMap[skillSO.levelSkill];
            m_currentHp -= dmg - def_ad;
            DIsplayDmg dIsplayDmg = Instantiate(dmgCanva, pontUiDmg.position,Quaternion.identity);
            dIsplayDmg.TakeDamage(dmg - def_ad);
            

        }
        if (skillSO.skillType == SkillType.MagicDmg)
        {
            int dmg = PlayerController.instance.player.GetMagicDmg();
            dmg += skillSO.levelDamageMap[skillSO.levelSkill];
            m_currentHp -= dmg - def_ap;
            DIsplayDmg dIsplayDmg = Instantiate(dmgCanva, pontUiDmg.position,Quaternion.identity);
            dIsplayDmg.TakeDamage(dmg - def_ap);
        }

        hpbar.value = (float)m_currentHp / HP;
        EnemyDeath();
    }

    private void EnemyDeath()
    {
        if (m_currentHp <= 0)
        {
            m_enemyAnimation.SetAniDie();
            GameController.instance.DropItem(this.transform);
            PlayerController.instance.ReWardExp(expReward);
            GameSave.SaveDataPlayer.SaveGold(GameSave.GetDataPlayer.GetGold() + goldReward);
            StartCoroutine(HandleDeath());
        }
    }

    private IEnumerator HandleDeath()
    {
        m_isMove = false;
        yield return new WaitForSeconds(2f);
        EnemyController.instance.Spawn(startPos);
        gameObject.SetActive(false);
    }
    private void Walk()
    {

        if (m_isMove)
        {
            m_enemyAnimation.SetAniStage(1);
            if (m_movingLeft)
            {
                // Di chuyển từ điểm B đến điểm A
                transform.position = Vector3.MoveTowards(transform.position, m_posA, moveSpeed * Time.deltaTime);
                transform.localScale = new Vector3(-scale.x, scale.y, scale.z);
                // Đổi hướng khi đạt đến điểm A
                if (transform.position.x <= m_posA.x)
                {
                    m_movingLeft = false;
                }
            }
            else
            {
                // Di chuyển từ điểm A đến điểm B
                transform.position = Vector3.MoveTowards(transform.position, m_posB, moveSpeed * Time.deltaTime);
                transform.localScale = new Vector3(scale.x, scale.y, scale.z);
                // Đổi hướng khi đạt đến điểm B
                if (transform.position.x >= m_posB.x)
                {
                    m_movingLeft = true;
                }
            }
        }
    }
    public void AttackPlayer()
    {
        m_enemyAnimation.SetAniAttack();
        m_isMove = false;
    }
    public void ExitAttack()
    {
        m_enemyAnimation.SetAniStage(1);
        m_isMove = true;
    }
    public void SharkSpawnArrow()
    {
        if (objectType == ObjectType.Monster2)
        {
            GameObject bullet = Instantiate(bulletAtk, transform.position, Quaternion.identity);
            Vector3 direction = m_player.transform.position - transform.position;
            float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
            angle -= 45f;
            bullet.transform.rotation = Quaternion.Euler(0, 0, angle);
        }
    }
}



