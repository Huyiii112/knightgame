using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAnimation : MonoBehaviour
{
    public Animator animator;

    
    public void SetAniStage(int stage)
    {
        animator.SetInteger("Stage", stage);
    }
    public void SetAniHurt()
    {
        animator.SetTrigger("Hurt");
    }
    public void SetAniAttack()
    {
        animator.SetTrigger("Attack");
    }
    public void SetAniDie()
    {
        animator.SetTrigger("Die");
    }
}
