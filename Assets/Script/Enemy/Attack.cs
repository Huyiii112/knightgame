using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attack : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("Player"))
        {

            PlayerController.instance.SetHpByDmg(5);
            PlayerController.instance.playerAnimation.Hurt();
        }
    }
}
