﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class DIsplayDmg : MonoBehaviour
{
    public TextMeshProUGUI text; 
    public float moveSpeed = 2f; 

    private void Update()
    {
        
        transform.position += new Vector3(0, moveSpeed * Time.deltaTime, 0);
    }
    public void TakeDamage(int damage)
    {
        text.text = "-" + damage.ToString();
        Destroy(gameObject, 0.5f);
    }
}
