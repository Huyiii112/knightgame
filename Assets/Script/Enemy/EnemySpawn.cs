using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawn : MonoBehaviour
{
    public EnemyPosSpawn pos;
    public ObjectType monsterType;
    public void Spawn()
    {
        for (int i = 0; i < pos.pos.Length; i++)
        {
            GameObject enemy = ObjectPoolController.Instance.GetObject(monsterType);
            enemy.transform.position = pos.pos[i].position;
            enemy.GetComponent<Enemy>().SetHp();

        }

    }
    public IEnumerator Spawn(Vector3 pos)
    {
        yield return new WaitForSeconds(5f);
        GameObject enemy = ObjectPoolController.Instance.GetObject(monsterType);
        enemy.transform.position = pos;
        enemy.GetComponent<Enemy>().startPos = pos;
        enemy.GetComponent<Enemy>().SetHp();
        enemy.GetComponent<Enemy>().SetPosMove();
    }

}

