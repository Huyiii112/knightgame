﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletShark : MonoBehaviour
{
    GameObject Player;
    public Vector3 direction;
    public float speedBullet;
    
    void Awake()
    {
        Player = GameObject.FindGameObjectWithTag("Player");
        direction = Player.transform.position - transform.position;
    }
    // Update is called once per frame
    void Update()
    {
        MoveTowardsPlayer();
    }
    void MoveTowardsPlayer()
    {
        if (Player)
        {
            gameObject.transform.position += direction * speedBullet * Time.deltaTime;
        }
        else
        {
            gameObject.SetActive(false);
            return;
        }
    }
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("Player"))
        {
            PlayerController.instance.playerAnimation.Hurt();
            PlayerController.instance.SetHpByDmg(7);
            gameObject.SetActive(false);

        }
    }
}
