﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    public static EnemyController instance;
    public EnemySpawn enemySpawn;

    private Queue<Vector3> spawnQueue = new Queue<Vector3>(); // Hàng đợi lưu các vị trí spawn
    private bool isProcessing = false; // Cờ kiểm tra coroutine đang chạy

    private void Awake()
    {
        instance = this;
    }
    private void Start()
    {
        enemySpawn.Spawn();
    }

    public void Spawn(Vector3 pos)
    {
        spawnQueue.Enqueue(pos); // Thêm vị trí spawn vào hàng đợi

        // Nếu chưa có coroutine nào đang chạy, bắt đầu xử lý hàng đợi
        if (!isProcessing)
        {
            StartCoroutine(ProcessQueue());
        }
    }

    private IEnumerator ProcessQueue()
    {
        isProcessing = true; // Đánh dấu coroutine đang chạy

        while (spawnQueue.Count > 0)
        {
            Vector3 pos = spawnQueue.Dequeue(); // Lấy vị trí đầu tiên trong hàng đợi
            yield return StartCoroutine(enemySpawn.Spawn(pos)); // Xử lý spawn
        }

        isProcessing = false; // Đặt lại cờ sau khi hoàn thành
    }
}
