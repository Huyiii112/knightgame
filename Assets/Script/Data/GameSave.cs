﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class GameSave
{
    public class SaveDataPlayer
    {
        public static void SaveIsSetHpMp(int set)
        {
            PlayerPrefs.SetInt(GameData.Player.ISSETHPMP, set);
        }
        public static void SaveGold(int set)
        {
            PlayerPrefs.SetInt(GameData.Player.GOLD_PLAYER, set);
        }
        public static void SaveMusicVollume(float set)
        {
            PlayerPrefs.SetFloat(GameData.Player.MUSICVOLLUME, set);
        }
        public static void SaveEffectVollume(float set)
        {
            PlayerPrefs.SetFloat(GameData.Player.EFFECTVOLLUME, set);
        }
    }
    public class GetDataPlayer
    {
        public static int GetIsSetHpMp()
        {
            return PlayerPrefs.GetInt(GameData.Player.ISSETHPMP);
        }
        public static int GetGold()
        {
            return PlayerPrefs.GetInt(GameData.Player.GOLD_PLAYER);
        }
        public static float GetMusicVollume()
        {
            return PlayerPrefs.GetFloat(GameData.Player.MUSICVOLLUME);
        }
        public static float GetEffectVollume()
        {
            return PlayerPrefs.GetFloat(GameData.Player.EFFECTVOLLUME);
        }
    }

    public class SaveDataJson
    {
        private static string savePath = Application.dataPath + "/Data/Player/playerData.json";

        public static void SaveData(PlayerModel playerData)
        {
            if (playerData == null)
            {
                Debug.LogError("PlayerData is null. Cannot save!");
                return;
            }

            string json = JsonUtility.ToJson(playerData, true);
            File.WriteAllText(savePath, json);
         
        }

        public static PlayerModel LoadData()
        {
            if (File.Exists(savePath))
            {
                string json = File.ReadAllText(savePath);
                PlayerModel playerData = new PlayerModel();
                JsonUtility.FromJsonOverwrite(json, playerData);
                Debug.Log("Data loaded from: " + savePath);
                return playerData;
            }
            else
            {
                Debug.LogWarning("Save file not found: " + savePath);
                return new PlayerModel(); 
            }
        }


    }
}
