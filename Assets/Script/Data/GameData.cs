using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameData
{
    public class Player
    {
        public const string ISSETHPMP = "isSetHpMp";
        public const string GOLD_PLAYER = "GoldPlayer";
        public const string MUSICVOLLUME = "MusicVollume";
        public const string EFFECTVOLLUME = "EffectVollume";
    }
    public class SceneName
    {
        public const string PLAYSCENE = "PlayScene";
        public const string LOADING = "LoadingScene";
        public const string MENUSCENE = "MenuScene";
        public const string NPCSCENE = "NPCScene";
    }
}

