using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropItem : MonoBehaviour
{
    public Item[] listItem;

    public void DropRandom(Transform point)
    {
        int random = Random.Range(0, listItem.Length);
        if(listItem[random].Drop())
        {
            Item item = Instantiate(listItem[random],point.position,Quaternion.identity);
            
        }
    }
}
