using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public static AudioManager Instance;
    public AudioClip eftSkill1, eftSkill2,click,jump,buy_sell;
    public AudioSource audioSource,musicMap;

    private void Awake()
    {
        Instance = this;
    }
    private void Update()
    {
        audioSource.volume = GameSave.GetDataPlayer.GetEffectVollume();
        musicMap.volume = GameSave.GetDataPlayer.GetMusicVollume();
    }
    private void Start()
    {
        audioSource.volume = GameSave.GetDataPlayer.GetEffectVollume();
        musicMap.volume = GameSave.GetDataPlayer.GetMusicVollume();
    }
    public void PlayAudio(AudioClip clip)
    {
        audioSource.PlayOneShot(clip);
    }    
}

