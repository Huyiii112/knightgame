using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public Transform startPoint;
    public static GameController instance;
    public EffectManager effectManager;
    public PanelInfo panelInfo;
    public DropItem dropItem;
    private void Awake()
    {
        instance = this;
    }
    public void DropItem(Transform point)
    {
        dropItem.DropRandom(point);
    }  
}
