using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "InventoryEquip", menuName = "Inventory system/InventoryEquip")]
public class InventoryEquip : ScriptableObject
{
    public List<ItemObject> containerEquipItem = new List<ItemObject>();  
}
