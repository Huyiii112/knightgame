﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class InventoryController : MonoBehaviour
{
    public static InventoryController Instance;
    private ItemObject m_itemSwap;
    public InventoryObject inventoryObject;
    public InventoryEquip inventoryEquip;
    private void Awake()
    {
        Instance = this;

    }
    #region Equip
    public void AddItemEquip(Item item, int index)
    {
        if (item.item.itemType == ItemType.PoitionHP)
        {
            if (InventoryController.Instance.FindItemUpdate(item.item.itemType) > 0)
            {

                PoitionSO poition = (PoitionSO)item.item;
                Debug.Log(item.item.itemType);
                ChangedItemUpdate(item.item.itemType, -1);
                PlayerController.instance.HeallingHpMP(poition.healingNumber, 0);
                return;
            }
            else return;
        }
        if (item.item.itemType == ItemType.PoitionMP)
        {
            if (InventoryController.Instance.FindItemUpdate(item.item.itemType) > 0)
            {
                PoitionSO poition = (PoitionSO)item.item;
                ChangedItemUpdate(item.item.itemType, -1);
                PlayerController.instance.HeallingHpMP(0, poition.healingNumber);
                return;
            }
            else return;
        }
        if (item.item.itemType == ItemType.UpdateSkillBook || item.item.itemType == ItemType.UpdateStone)
        {
            UI.Instance.MessageGame("không thể trang bị");
            return;
        }
        if (inventoryEquip.containerEquipItem.Count == 0)
        {
            inventoryEquip.containerEquipItem.Add(item.item);
            AddBonus(item.item);
        }
        else
        {
            bool itemAdded = false;
            for (int i = 0; i < inventoryEquip.containerEquipItem.Count; i++)
            {
                var existingItem = inventoryEquip.containerEquipItem[i];
                if (item.item.itemType == existingItem.itemType)
                {
                    m_itemSwap = inventoryEquip.containerEquipItem[i];
                    InventorySlot newItem = new InventorySlot(1, m_itemSwap);
                    inventoryObject.container.Add(newItem);
                    inventoryEquip.containerEquipItem[i] = item.item;
                    ReduceCS(newItem.item);
                    itemAdded = true;
                    AddBonus(item.item);
                    break;

                }
            }
            if (!itemAdded)
            {
                inventoryEquip.containerEquipItem.Add(item.item);
                AddBonus(item.item);
            }
        }
        DropItemInventory(index);
    }
    public void AddBonus(ItemObject item)
    {
        PlayerController.instance.player.AddAttack(item.atkBonus);
        PlayerController.instance.player.AddMagic(item.magicBonus);
        PlayerController.instance.player.AddDef(item.defBonus);
    }
    public void ReduceCS(ItemObject item)
    {
        PlayerController.instance.player.AddAttack(-item.atkBonus);
        PlayerController.instance.player.AddMagic(-item.magicBonus);
        PlayerController.instance.player.AddDef(-item.defBonus);
    }
    #endregion

    #region Inventory
    public void AddItemToInventory(ItemObject item, int amount)
    {
        bool hasItem = false;
        bool isFull;

        if (inventoryObject.container.Count >= 28) isFull = true;
        else isFull = false;

        if (!isFull)
        {

            for (int i = 0; i < inventoryObject.container.Count; i++)
            {
                if (inventoryObject.container[i].item == item && item.itemType == ItemType.PoitionHP
                || inventoryObject.container[i].item == item && item.itemType == ItemType.UpdateSkillBook
                || inventoryObject.container[i].item == item && item.itemType == ItemType.UpdateStone
                || inventoryObject.container[i].item == item && item.itemType == ItemType.PoitionMP)
                {
                    inventoryObject.container[i].AddAmount(amount);
                    hasItem = true;
                    break;
                }
            }
            if (!hasItem)
            {
                inventoryObject.container.Add(new InventorySlot(amount, item));
            }
        }
        else
        {
            Debug.Log("Full");
        }

    }
    #endregion
    public void UnEquipItem(ItemObject item)
    {

        for (int i = 0; i < inventoryEquip.containerEquipItem.Count; i++)
        {
            if (inventoryEquip.containerEquipItem[i] == item)
            {
                ReduceCS(item);
                inventoryEquip.containerEquipItem.Remove(item);
                AddItemToInventory(item, 1);
            }
        }

    }
    public void DropItemInventory(int index)
    {
        inventoryObject.container.RemoveAt(index);
    }

    public void SetCSStartGame()
    {
        if (inventoryEquip.containerEquipItem.Count <= 0) return;
        foreach (var item in inventoryEquip.containerEquipItem)
        {
            AddBonus(item);
        }
    }

    public int FindItemUpdate(ItemType itemType)
    {
        for (int i = 0; i < inventoryObject.container.Count; i++)
        {
            if (inventoryObject.container[i].item.itemType == itemType)
            {
                return inventoryObject.container[i].amount;

            }
        }
        return 0;
    }
    public void ChangedItemUpdate(ItemType itemType, int amount)
    {
        foreach (var item in inventoryObject.container)
        {

            if (item.item.itemType == itemType)
            {
                item.amount += amount;
                return;
            }
        }
    }
}
