using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour
{
    public ItemObject item;
    
    public ItemType itemType;
    private void Start()
    {
        if (!item) return;
        itemType = item.itemType;
        GetComponent<SpriteRenderer>().sprite = item.prefabs;
    }
    public bool Drop()
    {
        int random = Random.Range(0, 100);
        if (random <= item.dropRate)
        {
            return true;
        }
        return false;
    }

}
