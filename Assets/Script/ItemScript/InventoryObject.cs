using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "Inventory", menuName = "Inventory system/Inventory")]
public class InventoryObject : ScriptableObject
{
    public List<InventorySlot> container = new List<InventorySlot>();
}
[System.Serializable]

public class InventorySlot
{
    public ItemObject item;
    public int amount;
    public InventorySlot(int amount, ItemObject itemObject)
    {
        this.item = itemObject;
        this.amount = amount;
    }
    public void AddAmount(int value)
    {
        this.amount += value;
    }
}





