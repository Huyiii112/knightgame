using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonItem : MonoBehaviour
{
    public ItemObject item;

    public void RemoveItem()
    {
        item = null;
    }
}
