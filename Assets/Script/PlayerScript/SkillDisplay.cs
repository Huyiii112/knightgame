﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkillDisplay : MonoBehaviour
{
    public SkillSO skinSo;
    private void Start()
    {
        
        transform.GetComponent<Button>().onClick.AddListener(() =>
        {
            string destext = "Level skill: " + skinSo.levelSkill.ToString() + 
            "\n Mô tả: " + skinSo.description+
            "\n Sát thương: " + skinSo.levelDamageMap[skinSo.levelSkill];

            UI.Instance.PanelInfoSkill.HideInfo(skinSo.nameSkill, destext);
            UI.Instance.PanelInfoSkill.HidePanel(true);
            UI.Instance.PanelInfoSkill.skinSo = skinSo;
        });
    }
}
