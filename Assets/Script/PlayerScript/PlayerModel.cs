﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class LevelData
{
    public int level;
    public int experienceRequired;
}
[System.Serializable]
public class PlayerModel : MonoBehaviour
{
    public int intelligence;
    public int agility;
    public int strength;

    public int def;
    public int attackDmg;
    public int magicDmg;
    public int HP;
    public int MP;
    public int currentHp;
    public int currentMp;
    public int currentExp;
    public Vector3 pos;
    public int level;
    public List<LevelData> levels = new List<LevelData>
    {
        new LevelData{ level = 1, experienceRequired = 100 },
        new LevelData{ level = 2, experienceRequired = 500 },
        new LevelData{ level = 3, experienceRequired = 700 },
        new LevelData{ level = 4, experienceRequired = 1000 },
        new LevelData{ level = 5, experienceRequired = 1400 },
        new LevelData{ level = 6, experienceRequired = 1700 },
        new LevelData{ level = 7, experienceRequired = 2000 },
        new LevelData{ level = 8, experienceRequired = 2500 },
        new LevelData{ level = 9, experienceRequired = 3000 },
        new LevelData{ level = 10, experienceRequired = 4000 }
    };
}
