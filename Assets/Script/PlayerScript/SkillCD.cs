using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SkillCD : MonoBehaviour
{
    public float cooldownTime;
    public Image imageCooldown;
    public TMP_Text textTime;
    public float time;
    public bool isCooldown;
    public SkillType type;
    private void Start()
    {
        imageCooldown.fillAmount = 0f;
        imageCooldown.gameObject.SetActive(false);
    }
    private void Update()
    {
        if (isCooldown)
        {
            imageCooldown.gameObject.SetActive(true);
            time -= Time.deltaTime;
            imageCooldown.fillAmount = time;
            textTime.text = time.ToString("#.##");
            if (time <= 0)
            {
                isCooldown = false;
                time = 0;
                imageCooldown.gameObject.SetActive(false);
            }
        }

    }

    public void CooldownSkill()
    {
        isCooldown = true;
        time = cooldownTime;
    }
}
