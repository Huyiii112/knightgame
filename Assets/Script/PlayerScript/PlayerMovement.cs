﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float speed;
    public Vector3 movement;
    
    public bool turnLeft;
    public float horizontalInput;
    public float jumpHeight;
    public Rigidbody2D rb;
    private Camera m_cam;

    private bool m_isJump;
    private Vector3 distance;
    private Vector3 offset;
    private int jumpCount;
    private float m_lastHorizontal;
    private void Awake()
    {
        m_cam = Camera.main;
    }
    private void Start()
    {

    }
    void Update()
    {
        Movement();
        CameraFollow();
    }

    public void Movement()
    {

        movement = new Vector3(horizontalInput * speed, rb.velocity.y, 0f) * Time.deltaTime;
        TurnLeft();
        transform.position += movement;
    }

    public void TurnLeft()
    {
        turnLeft = true;
        if (movement.x != 0) m_lastHorizontal = movement.x;
        if (m_lastHorizontal < 0) turnLeft = false;
        Vector3 scale = transform.localScale;

        if (turnLeft) scale.x = 2f;
        else scale.x = -2f;

        transform.localScale = scale;
    }
    public void MoveLeft()
    {
        horizontalInput = -1f;
        PlayerController.instance.playerAnimation.MovementAni(true);
    }
    public void MoveRight()
    {
        horizontalInput = 1f;
        PlayerController.instance.playerAnimation.MovementAni(true);
    }
    public void Idle()
    {
        horizontalInput = 0;
        PlayerController.instance.playerAnimation.IdleAni(true);
        PlayerController.instance.playerAnimation.MovementAni(false);
    }

    public void Jump()
    {
        if (m_isJump)
        {
            jumpCount = 0; // Reset jump count when grounded
        }

        // Jumping logic
        if (m_isJump || jumpCount < 2)
        {
            AudioManager.Instance.PlayAudio(AudioManager.Instance.jump);
            rb.velocity = new Vector2(rb.velocity.x, jumpHeight); 
            jumpCount++;
            PlayerController.instance.playerAnimation.JumpAni();
        }

    }
    public void CameraFollow()
    {
        offset = transform.position - m_cam.transform.position;
        distance = m_cam.transform.position + offset;

        m_cam.transform.position = new Vector3(distance.x, m_cam.transform.position.y, m_cam.transform.position.z);
    }


    private void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.collider.tag == "Matdat")
        {
            m_isJump = true;
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Matdat"))
        {
            m_isJump = true;
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Matdat"))
        {
            m_isJump = false;
        }
    }

}
