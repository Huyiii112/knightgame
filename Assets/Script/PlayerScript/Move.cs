﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Move : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public bool isButtonHeld = false;
    public float horizontalInput;

    public void OnPointerDown(PointerEventData eventData)
    {
        isButtonHeld = true;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        isButtonHeld = false;
        horizontalInput = 0f;
    }

    private void Update()
    {
        if (isButtonHeld)
        {
            
            PlayerController.instance.playerMovement.horizontalInput = horizontalInput;
            PlayerController.instance.playerMovement.Movement();
        }
        else
        {

        } 
    }
}
