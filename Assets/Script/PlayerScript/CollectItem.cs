﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectItem : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        var item = collision.GetComponent<Item>();

        if (item)
        {
            if (InventoryController.Instance.inventoryObject.container.Count < 28)
            {
                InventoryController.Instance.AddItemToInventory(item.item, 1);
                collision.gameObject.SetActive(false);
            }
            else
            {
                UI.Instance.MessageGame("Hành trang đầy!!!");
            }

        }

    }

}
