﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Player : MonoBehaviour
{
    [Header("Slider")]

    public Slider sliderHp;
    public Slider sliderMp;
    public Slider sliderExp;
    public int currentLevel;
    [Header("HP MP")]
    public int MaxHpPlayer;
    public int MaxMpPlayer;
    public int MaxExpLevel;
    [SerializeField]
    private int m_currentHp;
    [SerializeField]
    private int m_currentMp;
    [SerializeField]
    private int m_currentExp;


    [Header("Chỉ số")]
    [SerializeField]
    private int m_int;
    [SerializeField]
    private int m_agi;
    [SerializeField]
    private int m_str;

    [Header("Thông tin")]
    [SerializeField]
    private int m_attackDmg;
    [SerializeField]
    private int m_magicDmg;
    [SerializeField]
    private int m_def;

    public PlayerModel playerModel;
    private void Start()
    {
        SetHpMpStart();
        SetMagicDmg();
        SetAttackDmg();
        SetDef();
        InventoryController.Instance.SetCSStartGame();
        StartSlider();
        MaxExpLevel = MaxExpLevel = playerModel.levels[currentLevel].experienceRequired;

    }
    private void Update()
    {
        LoadInfo();
        SaveDataToSO();
        SliderExp();
    }

    #region Set HP MP
    public void SetHpMpStart()
    {

        if (GameSave.GetDataPlayer.GetIsSetHpMp() == 1)
        {
            LoadDataFromSO(GameSave.SaveDataJson.LoadData());
            return;
        }
        this.MaxHpPlayer = m_str * 10;
        m_currentHp = MaxHpPlayer;
        this.MaxMpPlayer = m_int * 10;
        m_currentMp = MaxMpPlayer;
        GameSave.SaveDataPlayer.SaveIsSetHpMp(1);
    }
    #endregion

    #region Get Set chỉ số
    //SET
    public void SetMagicDmg()
    {
        this.m_magicDmg = m_int * 5;
    }
    public void SetAttackDmg()
    {
        this.m_attackDmg = m_str * 5;
    }
    public void SetDef()
    {
        this.m_def = m_agi * 5;
    }
    // GET
    public int GetMagicDmg()
    {
        return this.m_magicDmg;
    }
    public int GetAttackDmg()
    {
        return this.m_attackDmg;
    }
    public int GetDef()
    {
        return this.m_def;
    }
    public int GetCurrentMp()
    {
        return m_currentMp;
    }
    #endregion

    #region Set TT
    public int Intelligence
    {
        get { return m_int; }
        set { m_int = value; }
    }

    // Hàm getter và setter cho m_agi
    public int Agility
    {
        get { return m_agi; }
        set { m_agi = value; }
    }

    // Hàm getter và setter cho m_str
    public int Strength
    {
        get { return m_str; }
        set { m_str = value; }
    }
    #endregion


    #region Save
    #endregion
    public void LoadInfo()
    {
        GameController.instance.panelInfo.SetInfo(m_str, m_int, m_agi, m_currentHp, m_currentMp, m_attackDmg, m_magicDmg, m_def, currentLevel, GameSave.GetDataPlayer.GetGold());
    }
    public void SaveDataToSO()
    {
        playerModel.strength = m_str;
        playerModel.intelligence = m_int;
        playerModel.agility = m_agi;
        playerModel.def = m_def;
        playerModel.magicDmg = m_magicDmg;
        playerModel.attackDmg = m_attackDmg;
        playerModel.HP = MaxHpPlayer;
        playerModel.MP = MaxMpPlayer;
        playerModel.currentHp = m_currentHp;
        playerModel.currentMp = m_currentMp;
        playerModel.level = currentLevel;
        playerModel.currentExp = m_currentExp;
        GameSave.SaveDataJson.SaveData(playerModel);
    }
    public void LoadDataFromSO(PlayerModel player)
    {
        m_str = player.strength;
        m_int = player.intelligence;
        m_agi = player.agility;
        m_def = player.def;
        m_magicDmg = player.magicDmg;
        m_attackDmg = player.attackDmg;
        MaxHpPlayer = player.HP;
        MaxMpPlayer = player.MP;
        m_currentHp = player.currentHp;
        m_currentMp = player.currentMp;
        currentLevel = player.level;
        m_currentExp = player.currentExp;
    }
    public void AddIntelligence(int _int)
    {
        this.m_int += _int;
    }
    public void AddStreght(int str)
    {
        this.m_str += str;
    }
    public void AddDef(int def)
    {
        m_def += def;
    }
    public void AddAttack(int atk)
    {
        m_attackDmg += atk;

    }
    public void AddMagic(int magic)
    {
        m_magicDmg += magic;
    }
    public void StartSlider()
    {
        sliderHp.value = (float)m_currentHp / MaxHpPlayer;
        sliderMp.value = (float)m_currentMp / MaxMpPlayer;

    }
    public void SetHpByDmg(int dmg)
    {
        m_currentHp -= dmg;
        if (m_currentHp < 0) m_currentHp = 0;
        sliderHp.value = (float)m_currentHp / MaxHpPlayer;
        if (m_currentHp <= 0)
        {
            PlayerController.instance.playerAnimation.Die();
            m_currentHp = MaxHpPlayer;
            m_currentMp = MaxMpPlayer;
        }
    }
    public void SetMpBySkill(int mp)
    {
        m_currentMp -= mp;
        if (m_currentMp < 0) m_currentMp = 0;
        sliderMp.value = (float)m_currentMp / MaxMpPlayer;
    }
    public void HeallingHp(int hp)
    {
        m_currentHp += hp;
        if (m_currentHp > MaxHpPlayer) m_currentHp = MaxHpPlayer;
        sliderHp.value = (float)m_currentHp / MaxHpPlayer;
    }
    public void HeallingMp(int mp)
    {
        m_currentMp += mp;
        if (m_currentMp > MaxMpPlayer) m_currentMp = MaxMpPlayer;
        sliderMp.value = (float)m_currentMp / MaxMpPlayer;
    }


    public void Exp(int exp)
    {
        m_currentExp += exp;
        if (m_currentExp >= MaxExpLevel) LevelUp();
    }
    public void SliderExp()
    {
        sliderExp.value = (float)m_currentExp / MaxExpLevel;
    }

    public void LevelUp()
    {
        m_currentExp = 0;
        currentLevel++;
        playerModel.level = currentLevel;
        UpdateCS();
        MaxExpLevel = playerModel.levels[currentLevel].experienceRequired;
    }
    public void UpdateCS()
    {
        m_agi += 1;
        m_int += 2;
        m_str += 1;
        this.MaxHpPlayer = m_str * 10;
        m_currentHp = MaxHpPlayer;
        this.MaxMpPlayer = m_int * 10;
        m_currentMp = MaxMpPlayer;
        SetMagicDmg();
        SetAttackDmg();
        SetDef();
        StartSlider();
    }
    public void DestroyPlayer()
    {

        Destroy(gameObject);
        LoadingScene.LoadScene(GameData.SceneName.NPCSCENE);
    }
}
