using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkillManager : MonoBehaviour
{
    public Button[] btnSklill;
    public static SkillManager Instance;
    public SkillCD[] listSkill;
    private void Awake()
    {
        Instance = this;
    }
    void Start()
    { 
        btnSklill[0].onClick.AddListener(() => { EnableSkill1(); });
        btnSklill[1].onClick.AddListener(() => { EnableSkill2(); });
        //btnSklill[2].onClick.AddListener(() => { EnableSkill3(); });
    }

    // Update is called once per frame
    public void EnableSkill1()
    {
       
        StartCoroutine(PlayerController.instance.playerAttack.Skill1());
        listSkill[0].CooldownSkill();
    }
    public void EnableSkill2()
    {
        PlayerController.instance.playerAttack.Skill2();
        listSkill[1].CooldownSkill();
    }
    //public void EnableSkill3()  
    //{

    //    listSkill[2].CooldownSkill();
    //}
}
