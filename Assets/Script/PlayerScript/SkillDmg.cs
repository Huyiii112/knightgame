using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillDmg : MonoBehaviour
{
    public SkillSO skill;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("Enemy"))
        {
            Enemy enemy = collision.GetComponent<Enemy>();
            enemy.DMG(skill);
        }    
    }
}
