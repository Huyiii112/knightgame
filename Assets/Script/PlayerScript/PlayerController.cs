using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public static PlayerController instance;
    public PlayerAnimation playerAnimation;
    public PlayerAttack playerAttack;
    public PlayerMovement playerMovement;
    public Player player;
    private void Awake()
    {
        instance = this;
    }
    public void MpUserSkill(int mp)
    {
        player.SetMpBySkill(mp);
    }
    public void SetHpByDmg(int dmg)
    {
        player.SetHpByDmg(dmg);
    }
    public void HeallingHpMP(int hp,int mp)
    {
        player.HeallingHp(hp);
        player.HeallingMp(mp);
    }  
    public void ReWardExp(int exp)
    {
        player.Exp(exp);
    }    
}
