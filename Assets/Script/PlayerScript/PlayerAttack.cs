﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;

public class PlayerAttack : MonoBehaviour
{
    public Transform point1;
    public Transform hitBox;
    public float detectionRange = 5f;
    public LayerMask enemyLayer;
    public GameObject arrowPrefab;
    private GameObject m_currentArrow;
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        DetectEnemiesInRange();
    }


    void DetectEnemiesInRange()
    {
        // Tạo một vòng tròn phát hiện xung quanh player
        Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, detectionRange, enemyLayer);
        float minDistance = Mathf.Infinity;
        Transform closestEnemy = null;

        foreach (Collider2D collider in colliders)
        {
            Enemy enemy = collider.GetComponent<Enemy>();
            if (enemy != null)
            {
                float distance = Vector2.Distance(transform.position, enemy.transform.position);
                if (distance < minDistance)
                {
                    minDistance = distance;
                    closestEnemy = enemy.transform;
                }
            }
        }

        if (closestEnemy != null)
        {

            point1 = closestEnemy;


            if (m_currentArrow == null)
            {
                m_currentArrow = Instantiate(arrowPrefab);
            }


            m_currentArrow.transform.position = closestEnemy.position + Vector3.up * 1f;
            m_currentArrow.transform.up = (closestEnemy.position - transform.position).normalized;
        }
        else
        {
            // Xóa mũi tên nếu không có enemy
            if (m_currentArrow != null)
            {
                Destroy(m_currentArrow);
            }
        }
    }



    // Hiển thị vòng tròn phát hiện trong Scene để dễ dàng kiểm tra
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, detectionRange);
    }
    public IEnumerator Skill1()
    {
        AudioManager.Instance.PlayAudio(AudioManager.Instance.eftSkill1);
        PlayerController.instance.playerAnimation.AttackPhysicsAni();
        hitBox.gameObject.SetActive(true);
        yield return new WaitForSeconds(0.5f);
        hitBox.gameObject.SetActive(false);
    }

    public void Skill2()
    {
        if (PlayerController.instance.player.GetCurrentMp() >= 10)
        {
            AudioManager.Instance.PlayAudio(AudioManager.Instance.eftSkill2);
            PlayerController.instance.MpUserSkill(10);
            PlayerController.instance.playerAnimation.AttackMagicsAni();
            GameObject skill2 = Instantiate(GameController.instance.effectManager.skill2, point1.position, Quaternion.identity);
            Destroy(skill2, 1f);
        }
    }

}
