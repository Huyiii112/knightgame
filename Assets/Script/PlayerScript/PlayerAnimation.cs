using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimation : MonoBehaviour
{
 
    public Animator animator;
    public void Start()
    {
        animator = GetComponent<Animator>();
    }
    public void AttackPhysicsAni()
    {
        animator.SetTrigger("Attack");
    }
    public void IdleAni(bool idle)
    {
        animator.SetBool("Idle", idle);
    }
    public void AttackMagicsAni()
    {
        animator.SetTrigger("AttackMagic");
    }
    public void MovementAni(bool walk)
    {
        animator.SetBool("Walk", walk);
    }
    public void JumpAni()
    {
        animator.SetTrigger("Jump");
    }
    public void Die()
    {
        animator.SetTrigger("Die");
    }
    public void Hurt()
    {
        animator.SetTrigger("Hurt");
    }
}

