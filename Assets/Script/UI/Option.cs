using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Option : MonoBehaviour
{
    public Slider musicVollume;
    public Slider effectVollume;
    public Button cancel;

    private void Start()
    {
        effectVollume.value = GameSave.GetDataPlayer.GetEffectVollume();
        musicVollume.value = GameSave.GetDataPlayer.GetMusicVollume();
        cancel.onClick.AddListener(() => { CancelOption(); });
    }

    private void Update()
    {
        SetMusicVollume();
        SetEffectVollume();
    }

    public void CancelOption()
    {
        gameObject.SetActive(false);
    }    
    public void SetMusicVollume()
    {
        GameSave.SaveDataPlayer.SaveMusicVollume(musicVollume.value);
    }
    public void SetEffectVollume()
    {
        GameSave.SaveDataPlayer.SaveEffectVollume(effectVollume.value);
    }
}
