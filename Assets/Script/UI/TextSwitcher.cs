﻿using UnityEngine;
using UnityEngine.UI; 
using TMPro;

public class TextSwitcher : MonoBehaviour
{
    public TextMeshProUGUI uiText; 
    public string[] textArray; 
    private int m_currentIndex = 0; 

    void Start()
    {
      
        if (textArray.Length > 0)
        {
            uiText.text = textArray[m_currentIndex];
        }
    }

    void Update()
    {
        
        if (Input.GetMouseButtonDown(0)) 
        {
            ShowNextText();
        }
    }

    void ShowNextText()
    {
        // Chuyển sang đoạn text tiếp theo
        m_currentIndex++;

        if (m_currentIndex < textArray.Length)
        {
            uiText.text = textArray[m_currentIndex];
        }
        else
        {
            LoadingScene.LoadScene(GameData.SceneName.NPCSCENE);
        }
    }
}
