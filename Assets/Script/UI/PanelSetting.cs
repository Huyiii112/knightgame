using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PanelSetting : MonoBehaviour
{
    public Button buttonExitMenu, buttonCancel, buttonOption;
    public GameObject option;
    void Start()
    {
        buttonOption.onClick.AddListener(() => { Option(); });
        buttonCancel.onClick.AddListener(() => { Cancel(); });
        buttonExitMenu.onClick.AddListener(() => { ExitMenu(); });
    }


    public void Option()
    {
        option.SetActive(true);
    }
    public void Cancel()
    {
        Time.timeScale = 1;
        gameObject.SetActive(false);
    }
    public void ExitMenu()
    {
        Time.timeScale = 1;
        LoadingScene.LoadScene(GameData.SceneName.MENUSCENE);
    }
}
