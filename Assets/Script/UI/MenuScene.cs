using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuScene : MonoBehaviour
{
    public Button btnStart;
    public GameObject panelText;
    void Start()
    {
        btnStart.onClick.AddListener(()=> { StartGame(); });
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void StartGame()
    {
        if(GameSave.GetDataPlayer.GetIsSetHpMp() == 0)
        {
            panelText.SetActive(true);
            gameObject.SetActive(false);
            return;
        }
        LoadingScene.LoadScene(GameData.SceneName.NPCSCENE);
    }
}
