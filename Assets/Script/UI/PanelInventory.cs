﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PanelInventory : MonoBehaviour
{
    public Button[] btnItems;
    public Button btnBag, btnSkill;
    public Sprite defauldImg;
    public PanelInfoItem panelButtonItem;
    public Button btnDrop, btnEquipment;
    public PanelInfoItem displayItemEquip;
    public Image prefabs;
    public Item itemSelected;

    public GameObject skill;
    public GridLayoutGroup bag;

    private int indexButton;

    private void OnEnable()
    {
        UpdateInventory();
    }


    public void UpdateInventory()
    {

        for (int i = 0; i < InventoryController.Instance.inventoryObject.container.Count; i++)
        {
            Image item_current = Instantiate(prefabs);

            item_current.sprite = InventoryController.Instance.inventoryObject.container[i].item.prefabs;
            item_current.transform.SetParent(btnItems[i].transform, false);
            item_current.rectTransform.localScale = new Vector3(0.7f, 0.7f);
            item_current.raycastTarget = false;
            btnItems[i].GetComponent<ButtonItem>().item = InventoryController.Instance.inventoryObject.container[i].item;
        }
        for (int i = InventoryController.Instance.inventoryObject.container.Count; i < 28; i++)
        {
            btnItems[i].GetComponent<ButtonItem>().item = null;
        }

    }

    private void Start()
    {
        btnDrop.onClick.AddListener(() => { DropItem(); });
        btnEquipment.onClick.AddListener(() => { EquipmentItem(); });
        for (int i = 0; i < btnItems.Length; i++)
        {
            int index = i;
            btnItems[i].onClick.AddListener(() => { InfoItems(btnItems[index].GetComponent<ButtonItem>()); });
        }
        btnBag.onClick.AddListener(() => { EnableBag(); });
        btnSkill.onClick.AddListener(() => { EnableSkillList(); });
    }
    public void EnableBag()
    {
        AudioManager.Instance.PlayAudio(AudioManager.Instance.click);
        bag.gameObject.SetActive(true);
        skill.SetActive(false);
        displayItemEquip.gameObject.SetActive(false);
        panelButtonItem.gameObject.SetActive(false);
        UI.Instance.PanelInfoSkill.HidePanel(false);
    }
    public void EnableSkillList()
    {
        AudioManager.Instance.PlayAudio(AudioManager.Instance.click);
        bag.gameObject.SetActive(false);
        skill.SetActive(true);
        displayItemEquip.gameObject.SetActive(false);
        panelButtonItem.gameObject.SetActive(false);
    }
    public void InfoItems(ButtonItem btn)
    {
        AudioManager.Instance.PlayAudio(AudioManager.Instance.click);
        if (btn.item == null) return;
        itemSelected.item = btn.item;
        itemSelected.itemType = btn.item.itemType;
        
        if (itemSelected.item)
        {
            indexButton = int.Parse(btn.name);
            panelButtonItem.item = itemSelected.item;
            displayItemEquip.gameObject.SetActive(false);
            panelButtonItem.gameObject.SetActive(true);
        }else
        {
            displayItemEquip.gameObject.SetActive(false);
            panelButtonItem.gameObject.SetActive(false);
        }
    }
    public void DropItem()
    {
       
        DesTroy();
        panelButtonItem.gameObject.SetActive(false);
        InventoryController.Instance.DropItemInventory(indexButton);
        UpdateInventory();
    }

    public void EquipmentItem()
    {

        DesTroy();
        panelButtonItem.gameObject.SetActive(false);
        InventoryController.Instance.AddItemEquip(itemSelected,indexButton);
        UpdateInventory();

    }

    public void DesTroy()
    {
        for (int i = 0; i < btnItems.Length; i++)
        {
            foreach (Transform child in btnItems[i].transform)
            {
                Destroy(child.gameObject);
            }

        }

    }



}
