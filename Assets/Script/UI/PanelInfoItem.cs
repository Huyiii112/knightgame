﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class PanelInfoItem : MonoBehaviour
{
    public ItemObject item;
    public TextMeshProUGUI nameItem;
    public TextMeshProUGUI description;

    private void OnDisable()
    {
        gameObject.SetActive(false);
    }
    private void Update()
    {
        nameItem.text = item.nameItem;
        if (item.itemType == ItemType.UpdateSkillBook || item.itemType == ItemType.UpdateStone
            || item.itemType == ItemType.PoitionHP || item.itemType == ItemType.PoitionMP)
        {
            description.text = item.description + 
                "\n Số lượng: " + InventoryController.Instance.FindItemUpdate(item.itemType).ToString();
        }else
        {
            description.text = item.description;
        }    
    }
   
}
