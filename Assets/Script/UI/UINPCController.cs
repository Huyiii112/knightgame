using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UINPCController : MonoBehaviour
{
    public static UINPCController instance;
    public PanelNpcPoition panelNpcPoition;
    public Item itemShopPoitionSelected;
    public PanelNpcPoition panelNpcWeapon;
    private void Awake()
    {
        instance = this;
    }

    public void HideNpcPoition(bool hide)
    {
        panelNpcPoition.gameObject.SetActive(hide);
    }     
    public void SetInfoItem()
    {
        panelNpcPoition.SetInfo(itemShopPoitionSelected);
        panelNpcWeapon.SetInfo(itemShopPoitionSelected);
    }    
}
