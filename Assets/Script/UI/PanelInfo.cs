﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class PanelInfo : MonoBehaviour
{

    private Button m_btn_select_private;
    public PanelInventory panelInventory;

    [Header("Trang bị")]
    public ItemObject[] itemObject;



    public Button[] btnItem;
    public Sprite defauldImg;
    public PanelInfoItem panelInfoItem;
    public Button unEqiup;

    [Header("Info")]
    public TextMeshProUGUI str, intelligence, agi, hp, mp, attack, magic, def,level,gold;
    public Image avatar;

    public PanelInfoItem PanelInfoItem;
    private ItemObject m_currentItem;
    public Image defauldItem;
    private void Start()
    {
        for (int i = 0; i < btnItem.Length; i++)
        {
            int index = i;
            btnItem[i].onClick.AddListener(() => { InfoItems(btnItem[index]); });
            unEqiup.onClick.AddListener(() => { UnEquipItem(btnItem[index]); });
        }

    }

    private void Update()
    {
        UpdateEquip();
    }

    public void UpdateEquip()
    {
        foreach (var item in InventoryController.Instance.inventoryEquip.containerEquipItem)
        {
           
            bool itemFound = false;
            for (int i = 0; i < btnItem.Length; i++)
            {
                if (item.itemType == btnItem[i].GetComponent<DisplayItemEquip>().ItemType)
                {
                    DesTroy(btnItem[i]);
                    Image current_item = Instantiate(defauldItem);

                    current_item.sprite = item.prefabs;
                    current_item.transform.SetParent(btnItem[i].transform, false);

                    current_item.rectTransform.localScale = new Vector3(0.7f, 0.7f);
                    current_item.raycastTarget = false;
                    btnItem[i].GetComponent<DisplayItemEquip>().itemEquip = item;
                    itemFound = true;
                    break;
                }
            }
            if (!itemFound)
            {
                Debug.LogWarning("Item type not found for an item in containerEquipItem. Setting default image.");
            }
        }
    }


    public void InfoItems(Button btn)
    {
        var currentItem = btn.GetComponent<DisplayItemEquip>().itemEquip;

        if (m_btn_select_private == btn)
        {
            PanelInfoItem.gameObject.SetActive(false);
        }
        m_currentItem = currentItem;
        if (currentItem)
        {
            PanelInfoItem.item = currentItem;
            panelInfoItem.gameObject.SetActive(false);
            PanelInfoItem.gameObject.SetActive(true);
        }
        m_btn_select_private = btn;
    }

    public void UnEquipItem(Button btn)
    {
        DesTroy(btn);
        InventoryController.Instance.UnEquipItem(m_currentItem);
        panelInventory.UpdateInventory();
        btn.GetComponent<DisplayItemEquip>().itemEquip = null;  
    }

    public void DesTroy(Button btn)
    {
        foreach (Transform child in btn.transform)
        {
            Destroy(child.gameObject);
        }

    }

    public void SetInfo(int str, int intelligence, int agi, int hp, int mp, int attack, int magic, int def,int level,int gold)
    {
        //this.avatar.sprite = avt;
        this.str.text = "STR: " + str.ToString();
        this.intelligence.text = "INT: " + intelligence.ToString();
        this.agi.text = "AGI: " + agi.ToString();
        this.hp.text = "HP: " + hp.ToString();
        this.mp.text = "MP: " + mp.ToString();
        this.attack.text = "AttackDmg: " + attack.ToString();
        this.magic.text = "MagicDmg: " + magic.ToString();
        this.def.text = "DEF: " + def.ToString();
        this.level.text = "Level: " + level.ToString();
        this.gold.text = "Gold: " + gold.ToString();

    }
}
