﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
public class UI : MonoBehaviour
{
    public Button buttonOpenInventory, btnSetting;
    public GameObject inventory, setting;
    public bool isOpenInventory;
    public static UI Instance;
    public TextMeshProUGUI messageInGame;
    public PanelInfoSkill PanelInfoSkill;
    private void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        isOpenInventory = true;
        buttonOpenInventory.onClick.AddListener(() => { EnableInventory(); });
        btnSetting.onClick.AddListener(() => { EnbaleSetting(); });
    }


    void Update()
    {

    }
    public void EnbaleSetting()
    {
        AudioManager.Instance.PlayAudio(AudioManager.Instance.click);
        Time.timeScale = 0;
        setting.SetActive(true);
    }

    public void EnableInventory()
    {
        AudioManager.Instance.PlayAudio(AudioManager.Instance.click);
        if (isOpenInventory)
        {
            inventory.SetActive(isOpenInventory);
            isOpenInventory = false;
        }
        else
        {
            inventory.SetActive(isOpenInventory);
            isOpenInventory = true;
        }
    }

    public void MessageGame(string message)
    {
        StartCoroutine(FadeOutText(message));
    }
    private IEnumerator FadeOutText(string message)
    {
        messageInGame.text = message;
        messageInGame.gameObject.SetActive(true);
        yield return new WaitForSeconds(0.5f);
        messageInGame.gameObject.SetActive(false);
    }
}
