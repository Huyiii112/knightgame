﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PanelInfoSkill : MonoBehaviour
{
    public TextMeshProUGUI skillName;
    public TextMeshProUGUI skillDescription;
    public Button upLevelSkill;
    public SkillSO skinSo;
    private void Start()
    {

        upLevelSkill.onClick.AddListener(() => { UpLevelSkill(); });
    }
    public void HideInfo(string skillName, string skillDescription)
    {
        this.skillName.text = skillName;
        this.skillDescription.text = skillDescription ;
    }
    public void UpLevelSkill()
    {
        if (skinSo == null) return;

        if (InventoryController.Instance.FindItemUpdate(ItemType.UpdateSkillBook) > 0)
        {
            skinSo.UpLevelSkill();
            string destext = "Level skill: " + skinSo.levelSkill.ToString() +
            "\n Mô tả: " + skinSo.description +
            "\n Sát thương: " + skinSo.levelDamageMap[skinSo.levelSkill];

            InventoryController.Instance.ChangedItemUpdate(ItemType.UpdateSkillBook, -1);
            HideInfo(skinSo.nameSkill, destext);
            HidePanel(true);
        }
    }

    public void HidePanel(bool hide)
    {
        transform.gameObject.SetActive(hide);
    }
}
