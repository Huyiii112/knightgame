using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "New Inventory Item", menuName = "Inventory/Weapon")]
public class WeaponSO : ItemObject
{
    private void Awake()
    {
        this.itemType = ItemType.Weapon;
    }
}
