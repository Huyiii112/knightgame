using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "New Inventory Item", menuName = "Inventory/Arrmor")]
public class ArrmorSO : ItemObject
{
    private void Awake()
    {
        this.itemType = ItemType.Arrmor;
    }
}
