using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "New Inventory Item", menuName = "Inventory/Necklace")]
public class NecklaceSO : ItemObject
{

    private void Awake()
    {
        this.itemType = ItemType.Necklace;
    }
}
