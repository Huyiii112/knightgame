using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
[CreateAssetMenu(fileName = "New Inventory Item", menuName = "Inventory/Poition")]
public class PoitionSO : ItemObject
{
    public int healingNumber;
}
