using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
[CreateAssetMenu(fileName = "Data", menuName = "PlayerSO/Player")]
public class PlayerSO : ScriptableObject
{
    public int intelligence;
    public int agility;
    public int strength;

    public int def;
    public int attackDmg;
    public int magicDmg;
    public int HP;
    public int MP;
    public int currentHp;
    public int currentMp;
    public int currentExp;
    public Vector3 pos;
    public int level;

    public  Dictionary<int, int> levels = new Dictionary<int, int>
    {
        { 1, 100 },
        { 2, 500 },
        { 3, 700 },
        { 4, 1000 },
        { 5, 1400 },
        { 6, 1700 },
        { 7, 2000 },
        { 8, 2500 },
        { 9, 3000 },
        { 10, 4000 }
    };
}
