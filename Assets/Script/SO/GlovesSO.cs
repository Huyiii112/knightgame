using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "New Inventory Item", menuName = "Inventory/Gloves")]
public class GlovesSO : ItemObject
{
    private void Awake()
    {
        this.itemType = ItemType.Gloves;
    }
}
