using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "New Inventory Item", menuName = "Inventory/Helmet")]
public class HelmetSO : ItemObject
{
    private void Awake()
    {
        this.itemType = ItemType.Helmet;
    }
}
