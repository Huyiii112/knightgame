using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "New Inventory Item", menuName = "Inventory/Shield")]
public class ShieldSO : ItemObject
{
    private void Awake()
    {
        this.itemType = ItemType.Shield;
    }
}
