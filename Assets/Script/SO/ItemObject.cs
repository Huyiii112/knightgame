using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class ItemObject : ScriptableObject
{
    public Sprite prefabs;
    public string nameItem;
    public ItemType itemType;
    [TextArea(15,20)]
    public string description;
    public int id;
    public int atkBonus;
    public int magicBonus;
    public int defBonus;
    public int dropRate;
    public int price;

}
