using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "New Inventory Item", menuName = "Inventory/Ring")]
public class RingSO : ItemObject
{
    private void Awake()
    {
        this.itemType = ItemType.Ring;
    }
}
