﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "New Skill")]
public class SkillSO : ScriptableObject
{
    public string nameSkill;
    public int levelSkill;
    public Object objectSkill;
    public string description;
    public SkillType skillType;
    
    public Dictionary<int, int> levelDamageMap = new Dictionary<int, int>()
    {
        { 1, 10 },
        { 2, 20 },
        { 3, 30 },
        { 4, 40 },
        { 5, 50 },
        { 6, 60 },
        { 7, 70 },
        { 8, 80 },

    };
    public void UpLevelSkill()
    {
       
        if (levelSkill < 8)
        {
            levelSkill += 1;
        }
        else

            UI.Instance.MessageGame("Đã đạt cấp tối đa!");
    }
}
