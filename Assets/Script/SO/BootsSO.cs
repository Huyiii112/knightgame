using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "New Inventory Item", menuName = "Inventory/Boots")]
public class BootsSO : ItemObject
{
    private void Awake()
    {
        this.itemType = ItemType.Boots;
    }
}
