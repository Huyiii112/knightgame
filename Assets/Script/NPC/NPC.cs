﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NPC : MonoBehaviour
{

    public GameObject shopUI;    
    public Button interactButton;  
    private bool isPlayerNearby = false;

    void Start()
    {
  
        shopUI.SetActive(false);
        interactButton.gameObject.SetActive(false);

      
        interactButton.onClick.AddListener(OpenShop);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player")) 
        {
            isPlayerNearby = true;
            interactButton.gameObject.SetActive(true); 
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Player")) 
        {
            isPlayerNearby = false;
            interactButton.gameObject.SetActive(false); 
            UINPCController.instance.itemShopPoitionSelected = null;
        }
    }

    void OpenShop()
    {
        shopUI.SetActive(isPlayerNearby);

    }

}
