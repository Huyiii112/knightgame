﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PanelNpcPoition : MonoBehaviour
{
    public Button btnBuy;
    public Item[] items;


    public TextMeshProUGUI textInfo;
    public Button btnClose;

    private void OnEnable()
    {
        textInfo.text = " ";
    }

    private void Start()
    {

        btnBuy.onClick.AddListener(() => { Buy(); });
        btnClose.onClick.AddListener(() => { CloseShop(false); });
    }

    public void CloseShop(bool hide)
    {
        gameObject.SetActive(hide);
    }
    public void Buy()
    {
        if (!UINPCController.instance.itemShopPoitionSelected) { UI.Instance.MessageGame("Hãy chọn vật phẩm muốn mua"); return; }
        if (GameSave.GetDataPlayer.GetGold() >= UINPCController.instance.itemShopPoitionSelected.item.price)
        {
            InventoryController.Instance.AddItemToInventory(UINPCController.instance.itemShopPoitionSelected.item, 1);
            UI.Instance.MessageGame("Mua thành công");
            AudioManager.Instance.PlayAudio(AudioManager.Instance.buy_sell);
        }
        else UI.Instance.MessageGame("Không đủ vàng");
    }
    public void SetInfo(Item item)
    {
        textInfo.text = item.item.name +
                        "\n\nGiá bán: " + item.item.price +
                        "\n\nCông dụng: " + item.item.description;
    }


}
