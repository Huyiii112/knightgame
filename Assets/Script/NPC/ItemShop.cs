﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemShop : MonoBehaviour
{
    public Item itemShop;
    private void Start()
    {
        transform.GetComponent<Button>().onClick.AddListener(() => { GetItem(); });
    }   
    public void GetItem()
    {
        UINPCController.instance.itemShopPoitionSelected = itemShop;
        UINPCController.instance.SetInfoItem();
    }    
    
   
}
